{-# LANGUAGE Rank2Types #-}

-- base
import Control.Monad (when)
import Data.Bits ((.&.))
import Data.Maybe (fromMaybe)
import Data.Semigroup ((<>))
import Data.Word (Word8)
import System.Exit (exitFailure)
import System.IO (stdin, stdout, stderr, IOMode(ReadMode), withFile, hPutStrLn)

-- bytestring
import Data.ByteString (hPut)

-- optparse-applicative
import Options.Applicative (ParserInfo, info, optional, strOption, long, short, help, metavar, fullDesc, progDesc, execParser)

-- pipes
import Pipes (lift, runEffect, Producer', yield, for, await, Pipe, (>->), each)

-- safe
import Safe (headMay)

-- local
import Command
import CoordinateDifference
import Decode
import Encode
import GroundTracking as GT
import Model
import Util

-- Scan from (x, y + 1, zl) to (x, y + 1, zh) operating under.
backScan :: (ND -> Command) -> Int -> Int -> Int -> Int -> Model -> [Command]
backScan op x y zl zh mdl =  concat [  [op (D 0 (-1) 0) | at (x, y, z) mdl]
                                 ++ [SMove (D 0 0 1)] | z <- [zl .. zh - 1]
                                 ]
                       ++ [op (D 0 (-1) 0) | at (x, y, zh) mdl]

-- Scan from (x, y + 1, zh) to (x, y + 1, zl) filling under.
fwdScan :: (ND -> Command) -> Int -> Int -> Int -> Int -> Model -> [Command]
fwdScan op x y zl zh mdl =  [op (D 0 (-1) 0) | at (x, y, zh) mdl]
                      ++ concat [  [SMove (D 0 0 (-1))]
                                ++ [op (D 0 (-1) 0) | at (x, y, z) mdl]
                                | z <- [zh - 1, zh - 2 .. zl]
                                ]

{-
(Currently BROKEN and unused. DO NOT USE!)

Scan through (x, y + 1, zl) to (x, y + 1, zh) moving in zd filling
(x - 1, y, zs) to (x + 1, y, ze) depending on lines and xd arguments.
-}
zScan3 :: Int -> Int -> Int -> Int -> Int -> Model -> [Command]
zScan3 x y zs zdi ze mdl =  [Fill (D 0 (-1)   0) |                              at (x, y, zs) mdl]
                         ++ [Fill (D 0 (-1) zdi) | let z' = zs + zdi, z' <= ze, at (x, y, z') mdl]
                         ++ concat [[SMove (D 0 0 zde)] ++ fill3 (x, y, z)
                                    | z <- drop 1 [zs, zs + zde .. ze - zdi]]
                         ++ [SMove (D 0 0 (zr * zdi)) | zl /= 0]
                         ++ [Fill (D 0 (-1) (negate zdi)) | zr > 2, at (x, y, ze - zdi) mdl]
                         ++ [Fill (D 0 (-1)           0 ) | zr > 1, at (x, y, ze      ) mdl]
 where
	zl = (ze - zs)
	zr = (zl * zdi - 1) `mod` 3 + 1
	zde = 3 * zdi
	fill3 (x, y, z) = concat
		[[Fill (D 0 (-1) (-1)) | at (x, y, z - 1) mdl]
		,[Fill (D 0 (-1)   0 ) | at (x, y, z    ) mdl]
		,[Fill (D 0 (-1)   1 ) | at (x, y, z + 1) mdl]
		]

-- Scan from (xl, y + 1, zl) to (xh, y + 1, zh) filling under.
rightScan :: (ND -> Command) -> Int -> Int -> Int -> Int -> Int -> Model -> [Command]
rightScan op xl xh y zl zh mdl = concat [zScan x ++ [SMove (D 1 0 0)] | x <- [xl .. xh - 1]] ++ zScan xh
 where
	zScan x | x .&. 1 == xl .&. 1 = backScan op x y zl zh mdl
	        | otherwise           =  fwdScan op x y zl zh mdl

{-
Scan from (xh, y + 1, zl) to (xl, y + 1, zh) filling
under.  Depending on resolution, might start in the back or the front.
-}
leftScan :: (ND -> Command) -> Int -> Int -> Int -> Int -> Int -> Model -> [Command]
leftScan op xl xh y zl zh mdl = zScan xh ++ concat [[SMove (D (-1) 0 0)] ++ zScan x | x <- [xh - 1, xh - 2 .. xl]]
 where
	zScan x | x .&. 1 /= xl .&. 1 = backScan op x y zl zh mdl
	        | otherwise           =  fwdScan op x y zl zh mdl

{-
Assemble the part of the model withing the specifed bounding box.  Assumes one
bot already just above the left, bottom, near corner of the box.  Ends on right
if odd number of layers; Ends in back if odd number of scanlines.
-}
upScan :: Coordinate -> Coordinate -> Model -> [Command]
upScan (xl, yl, zl) (xh, yh, zh) mdl =  concat [xScan y ++ [SMove (D 0 1 0)] | y <- [yl .. yh - 1]]
                                     ++ xScan yh
 where
	xScan y | y .&. 1 == yl .&. 1 = rightScan Fill xl xh y zl zh mdl
	        | otherwise           =  leftScan Fill xl xh y zl zh mdl

{-
Disassemble the part of the model within the specific bounding box. Assumes one
bot, already just above the left, top, near corner of the box.  Ends on right
if odd number of layers; ends in back is odd number of scanlines.
-}
downScan :: Coordinate -> Coordinate -> Model -> [Command]
downScan (xl, yl, zl) (xh, yh, zh) mdl = drop 1 $ concat [[SMove (D 0 (-1) 0)] ++ xScan y | y <- [yh, yh - 1 .. yl]]
 where
	xScan y | y .&. 1 == yh .&. 1 = rightScan Void xl xh y zl zh mdl
	        | otherwise           =  leftScan Void xl xh y zl zh mdl

{-
Tries to shrink an axis-aligned bounding box.  Moderately expensive, even on
failure.  If there's nothing in the input box, the output is the input box
turned "inside out".
-}
shrinkAabb :: Coordinate -> Coordinate -> Model -> (Coordinate, Coordinate)
shrinkAabb (l, b, n) (r, t, f) mdl = ((left, bottom, near), (right, top, far))
 where
	xs = [l .. r]
	ys = [b .. t]
	zs = [n .. f]
	left   = fromMaybe r (headMay [x | x <- xs, y <- ys, z <- zs, at (x, y, z) mdl])
	bottom = fromMaybe t (headMay [y | y <- ys, x <- xs, z <- zs, at (x, y, z) mdl])
	near   = fromMaybe f (headMay [z | z <- zs, x <- xs, y <- ys, at (x, y, z) mdl])
	right  = fromMaybe l (headMay [x | x <- reverse xs, y <- ys, z <- zs, at (x, y, z) mdl])
	top    = fromMaybe b (headMay [y | y <- reverse ys, x <- xs, z <- zs, at (x, y, z) mdl])
	far    = fromMaybe n (headMay [z | z <- reverse zs, x <- xs, y <- ys, at (x, y, z) mdl])

aabb :: Model -> (Coordinate, Coordinate)
aabb mdl = shrinkAabb (1, 0, 1) (limit, limit, limit) mdl
 where
	limit = resolution mdl - 2

aabbByLayer :: Model -> [(Coordinate, Coordinate)]
aabbByLayer mdl = map (flip (uncurry shrinkAabb) mdl) [((l, y, n), (r, y, f)) | y <- [b .. t]]
 where
	((l, b, n), (r, t ,f)) = aabb mdl

smoveCmds :: Axis -> Int -> [Command]
smoveCmds a i =  replicate nl (SMove $ ld a (s * 15))
              ++ replicate ns (SMove $ ld a (s * 5))
              ++ [SMove $ ld a (s * r) | r /= 0]
 where
	s = signum i
	(nl, rl) = abs i `divMod` 15
	(ns, r)  =    rl `divMod`  5

defaultStrategy :: Model -> Producer' Command IO ()
defaultStrategy mdl = each (  smoveCmds Y (bottom + 1)
                           ++ smoveCmds X left ++ smoveCmds Z near
                           ++ upScan lbn rtf mdl
                           ++ resetZ ++ resetX ++ resetY ++ [Halt]
                           ) >-> flipHandlerEmpty (resolution mdl)
 where
	(lbn@(left, bottom, near), rtf@(right, top, far)) = aabb mdl
	layers = top - bottom + 1
	linesPerLayer = right - left + 1
	scanlines = layers * linesPerLayer
	resetZ = smoveCmds Z (negate $ if scanlines .&. 1 == 1 then far else near)
	resetX = smoveCmds X (negate $ if layers .&. 1 == 1 then right else left)
	resetY = smoveCmds Y (negate $ top + 1)

disassemble :: Model -> Producer' Command IO ()
disassemble mdl = disassembleScan >-> flipHandlerNonempty mdl
 where
	(lbn@(left, bottom, near), rtf@(right, top, far)) = aabb mdl
	layers = top - bottom + 1
	linesPerLayer = right - left + 1
	scanlines = layers * linesPerLayer
	disassembleScan = do
		each . smoveCmds Y $ top + 1
		each $ smoveCmds X left
		each $ smoveCmds Z near
		each $ downScan lbn rtf mdl
		each . smoveCmds Z . negate $ if scanlines .&. 1 == 1 then far else near
		each . smoveCmds X . negate $ if layers .&. 1 == 1 then right else left
		each . smoveCmds Y . negate $ bottom + 1
		yield Halt

reassemble :: Model -> Model -> Producer' Command IO ()
reassemble src tgt = reassembleScan >-> flipHandlerNonempty src
 where
	(slbn@(sleft, sbottom, snear), srtf@(sright, stop, sfar)) = aabb src
	(tlbn@(tleft, tbottom, tnear), trtf@(tright, ttop, tfar)) = aabb tgt
	slayers = stop - sbottom + 1
	slinesPerLayer = sright - sleft + 1
	sscanlines = slayers * slinesPerLayer
	tlayers = ttop - tbottom + 1
	tlinesPerLayer = tright - tleft + 1
	tscanlines = tlayers * tlinesPerLayer
	reassembleScan = do
		each . smoveCmds Y $ stop + 1
		each $ smoveCmds X sleft
		each $ smoveCmds Z snear
		each $ downScan slbn srtf src
		each . smoveCmds Z $ tnear - (if sscanlines .&. 1 == 1 then sfar else snear)
		each . smoveCmds X $ tleft - (if slayers .&. 1 == 1 then sright else sleft)
		each . smoveCmds Y $ tbottom - sbottom
		each $ upScan tlbn trtf tgt
		each . smoveCmds Z . negate $ if tscanlines .&. 1 == 1 then tfar else tnear
		each . smoveCmds X . negate $ if tlayers .&. 1 == 1 then tright else tleft
		each . smoveCmds Y . negate $ ttop + 1
		yield Halt

ffError :: a
ffError = error "flipInserter doesn't support tracking multiple bots"

voidError :: a
voidError = error "flipInserter dooesn't support tracking subtractive methods"

flipHandlerStep :: Int -> Coordinate -> GroundTracking -> Command -> Producer' Command IO (Coordinate, GroundTracking)
flipHandlerStep _ bp gt      Halt             = yield Halt >> return (bp                , gt)
flipHandlerStep _ bp gt      Wait             = yield Wait >> return (bp                , gt)
flipHandlerStep _ bp gt      Flip             =               return (bp                , gt)
flipHandlerStep _ bp gt cmd@(SMove lld)       = yield cmd  >> return (bp .+ lld         , gt)
flipHandlerStep _ bp gt cmd@(LMove sld1 sld2) = yield cmd  >> return (bp .+ sld1 .+ sld2, gt)
flipHandlerStep _ bp gt cmd@(FusionP _)       = yield cmd  >> return (ffError           , gt) -- Either an error, or bp tracking is already wrong.
flipHandlerStep _ bp gt cmd@(FusionS _)       = yield cmd  >> return (ffError           , gt) -- Either an error, or bp tracking is already wrong.
flipHandlerStep _ bp gt cmd@(Fission _ _)     = yield cmd  >> return (ffError           , gt) -- Can't track multiple bots
flipHandlerStep _ bp gt cmd@(GFill _ _)       = yield cmd  >> return (ffError           , error "No fill update for regions, yet")
flipHandlerStep _ bp gt cmd@(GVoid _ _)       = yield cmd  >> return (ffError           , error "No void update for regions, yet")
flipHandlerStep _ bp gt cmd@(Void nd)         = do
	gt' <- lift $ voidUpdate (bp .+ nd) gt
	let willLow = canLow gt'
	when (wasLow && not willLow) $ yield Flip
	yield cmd
	when (not wasLow && willLow) $ yield Flip
	return (bp, gt')
 where
	wasLow = canLow gt
flipHandlerStep r bp gt cmd@(Fill nd)         = do
	gt' <- lift $ fillUpdate r (bp .+ nd) gt
	let willLow = canLow gt'
	when (wasLow && not willLow) $ yield Flip
	yield cmd
	when (not wasLow && willLow) $ yield Flip
	return (bp, gt')
 where
	wasLow = canLow gt

flipHandlerLoop :: Int -> Coordinate -> GroundTracking -> Pipe Command Command IO r
flipHandlerLoop r = flipHandlerLoop'
 where flipHandlerLoop' ibp igt = await >>= flipHandlerStep r ibp igt >>= uncurry flipHandlerLoop'

flipHandlerNonempty :: Model -> Pipe Command Command IO r
flipHandlerNonempty src = lift (GT.fromModel src) >>= flipHandlerLoop (resolution src) (0, 0, 0)

flipHandlerEmpty :: Int -> Pipe Command Command IO r
flipHandlerEmpty r = flipHandlerLoop r (0, 0, 0) GT.empty

data Options = Opts
	{ tgtFile :: Maybe FilePath
	, srcFile :: Maybe FilePath
	} deriving (Eq, Show, Read)

optParse :: ParserInfo Options
optParse = info (   Opts
                <$> optional (strOption (long "assemble" <> short 'a' <> help "Model to assemble" <> metavar "TGTFILE"))
                <*> optional (strOption (long "disassemble" <> short 'd' <> help "Model to disassemble" <> metavar "SRCFILE"))
                )
                (fullDesc <> progDesc "ICFPPC 2018 program by Team Junkfood")

game :: IO ()
game = do
	options <- execParser optParse
	case options of
	 Opts Nothing  Nothing -> return () -- Nothing to do
	 Opts (Just a) Nothing -> do
		tgt <- withFile a ReadMode hGetModel
		runEffect $ defaultStrategy tgt >-> streamWriteTrace (hPut stdout)
	 Opts Nothing (Just d) -> do
		src <- withFile d ReadMode hGetModel
		runEffect $ disassemble src >-> streamWriteTrace (hPut stdout)
	 Opts (Just a) (Just d) -> do
		tgt <- withFile a ReadMode hGetModel
		src <- withFile d ReadMode hGetModel
		runEffect $ reassemble src tgt >-> streamWriteTrace (hPut stdout)

main = game
