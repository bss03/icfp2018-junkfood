{-# LANGUAGE Rank2Types #-}

-- base
import Control.Monad (when)
import Data.Bits ((.&.))
import Data.Maybe (fromMaybe)
import Data.Semigroup ((<>))
import Data.Word (Word8)
import System.Exit (exitFailure)
import System.IO (stdin, stdout, stderr, IOMode(ReadMode), withFile, hPutStrLn)

-- bytestring
import Data.ByteString (hPut)

-- optparse-applicative
import Options.Applicative (ParserInfo, info, optional, strOption, long, short, help, metavar, fullDesc, progDesc, execParser)

-- pipes
import Pipes (lift, runEffect, Producer', yield, for, await, Pipe, (>->), each)

-- safe
import Safe (headMay)

-- local
import Command
import CoordinateDifference
import Decode
import Encode
import GroundTracking as GT
import Model
import Util

traceDecoder :: IO ()
traceDecoder = do
	runEffect $ for (hGetTrace stdin) (lift . print)

main = traceDecoder
