# icfp2018-junkfood

The submission from Team Junkfood for the International Conference on Functional Programming Programming Contest 2018.

# Building The Source

Use the `stack` tool to build the `nanobuild` commandline utility. If you have not set up stack before use `stack setup` (one time) to prepare for the build. Once the setup is complete use `stack build` to get a build of the `nanobuild` tool.

# Using `nanobuild`

The `nanobuild` program needs a model to output a build plan. You can generate one by using `stack exec -- nanobuild < modelfile.mdl`. This will output the plan to stdout. You probably don't want this. You probably want to dump this to a file like so, `stack exec -- nanobuild < modelfile.mdl > plan.nbt`.
