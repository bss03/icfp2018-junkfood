{-# LANGUAGE Rank2Types #-}
module CoordinateSet
	( CoordinateSet, empty, singleton, fromModel, union, unions
	, insert, delete, member, inOrAdjacent, null, size, anyGround, toList
	, adjacents
	, MCoordinateSet, resolution, emptyM, singletonM, fromModelM
	, insertM, deleteM, inOrAdjacentM, adjacentsM, anyGroundM, unionM, unions1M, toListM
	)
where

import Prelude hiding (null, any, read)

-- base
import Control.Applicative (liftA2)
import Control.Monad (when, guard, (>=>), forM_, filterM)
import Data.Bits (setBit, clearBit, testBit, (.|.), (.&.), unsafeShiftR, unsafeShiftL)
import Data.Foldable (any, find)
import Data.IORef (newIORef, atomicModifyIORef)
import Data.List (foldl1')
import Data.Maybe (fromMaybe)
import Data.Word (Word64)

-- bytestring
import qualified Data.ByteString as BS

-- containers
import           Data.IntMap (IntMap)
import qualified Data.IntMap as IM
import           Data.IntSet (IntSet)
import qualified Data.IntSet as IS

-- pipes
import Pipes (lift, Producer', yield)
import qualified Pipes.Prelude as PP

-- vector
import Data.Vector.Unboxed.Mutable (IOVector, read, modify)
import qualified Data.Vector.Unboxed.Mutable as V

-- (this package)
import Coordinate
import           Model hiding (resolution)
import qualified Model as M
import Util

(.<<.) = unsafeShiftL

type CoordinateSet = IntMap (IntMap IntSet)

data MCoordinateSet = MCS
	{ res :: Int
	, mat :: IOVector Word64
	}

empty :: CoordinateSet
empty = IM.empty

resolutionSize :: Int -> Int
resolutionSize r = (r * r * r + 63) .>>. 6

emptyM :: Int -> IO MCoordinateSet
emptyM r = MCS r <$> V.new (resolutionSize r)

singleton :: Coordinate -> CoordinateSet
singleton (x, y, z) = IM.singleton x . IM.singleton y $ IS.singleton z

singletonM :: Int -> Coordinate -> IO MCoordinateSet
singletonM r c = do
	cs <- emptyM r
	insertM c cs
	return cs

fromModel :: Model -> CoordinateSet
fromModel mdl = IM.fromAscList [(x, IM.fromAscList [(y, IS.fromAscList [z
                                                                        | z <- [1 .. limit], at (x, y, z) mdl])
                                                    | y <- [0 .. limit]])
                                | x <- [1 .. limit]]
 where
	limit = M.resolution mdl - 2

swap (x, y) = (y, x)

fromModelM :: Model -> IO MCoordinateSet
fromModelM mdl = do
	bs <- newIORef $ matrix mdl
	let read64 = do
		h <- atomicModifyIORef bs (swap . BS.splitAt 8)
		return $ foldl1' (.|.) [if i < BS.length h then word8To64 (BS.index h i) .<<. (3 * (7 - i)) else 0 | i <- [0 .. 7]]
	MCS r <$> V.replicateM sz read64
 where
	r = M.resolution mdl
	sz = (r * r * r + 63) .>>. 6
	word8To64 = fromIntegral

resolution :: MCoordinateSet -> Int
resolution (MCS r _) = r

coordToBitIndex :: Coordinate -> Int -> Int
coordToBitIndex (x, y, z) r = x * r * r + y * r + z

bitIndexToIndexes :: Int -> (Int, Int)
bitIndexToIndexes i = (i .>>. 6, i .&. 63)

coordToIndexes :: Coordinate -> Int -> (Int, Int)
coordToIndexes = (bitIndexToIndexes .) . coordToBitIndex

insert :: Coordinate -> CoordinateSet -> CoordinateSet
insert (x, y, z) = IM.alter (Just . maybe (IM.singleton y zsing) (IM.alter (Just . maybe zsing (IS.insert z)) y)) x
 where
	zsing = IS.singleton z

insertM :: Coordinate -> MCoordinateSet -> IO ()
insertM c (MCS r iov) = modify iov (flip setBit b) w
 where
	(w, b) = coordToIndexes c r

member :: Coordinate -> CoordinateSet -> Bool
member (x, y, z) = fromMaybe False . (fmap $ IS.member z) . (IM.lookup x >=> IM.lookup y)

memberM :: Coordinate -> MCoordinateSet -> IO Bool
memberM c (MCS r iov) = flip testBit b <$> read iov w
 where
	(w, b) = coordToIndexes c r

inOrAdjacent :: Coordinate -> CoordinateSet -> Bool
inOrAdjacent (x, y, z) = or <$> sequenceA [ maybe False (fmap or . sequenceA $ zipWith (\tz f -> maybe False tz . f)
                                                                     [or <$> sequenceA [IS.member z' | z' <- [z, z + 1, z - 1]], IS.member z, IS.member z]
                                                                     [IM.lookup y' | y' <- [y, y + 1, y - 1]]
                                                        ) . IM.lookup x
                                          , member (x + 1, y, z)
                                          , member (x - 1, y, z)
                                          ]

inOrAdjacentM :: Coordinate -> MCoordinateSet -> IO Bool
inOrAdjacentM c@(x, y, z) (MCS r iov) = or <$> sequenceA [flip testBit b <$> read iov w | i <- ci : [ci + o | o <- offs], let (w, b) = bitIndexToIndexes i]
 where
	ci = coordToBitIndex c r
	l = r - 1
	r2 = r * r
	offs = [r2 | x < l] ++ [negate r2 | x > 0]
	    ++ [r  | y < l] ++ [negate r  | y > 0]
	    ++ [1  | z < l] ++ [negate 1  | z > 0]

adjacents :: Coordinate -> CoordinateSet -> [Coordinate]
adjacents (x, y, z) cs =  filter (flip member cs) [(x + 1, y, z), (x - 1, y, z)]
                       ++ maybe [] (\yzs -> maybe [] (\zs -> map ((,,) x y) $ filter (flip IS.member zs) [z + 1, z - 1]
                                                     ) (IM.lookup y yzs)
                                            ++ ( map (flip ((,,) x) z)
                                               $ filter (maybe False (IS.member z) . flip IM.lookup yzs) [y + 1, y - 1]
                                               )
                                   ) (IM.lookup x cs)

adjacentsM :: Coordinate -> MCoordinateSet -> IO [Coordinate]
adjacentsM (x, y, z) mcs@(MCS r _) = filterM (flip memberM mcs) inRange
 where
	l = r - 1
	inRange = [(x + 1, y, z) | x < l] ++ [(x - 1, y, z) | x > 0]
	       ++ [(x, y + 1, z) | y < l] ++ [(x, y - 1, z) | y > 0]
	       ++ [(x, y, z + 1) | z < l] ++ [(x, y, z - 1) | z > 0]

delete :: Coordinate -> CoordinateSet -> CoordinateSet
delete (x, y, z) = IM.alter (>>= purify (not . IM.null) . IM.alter (>>= purify (not . IS.null) . IS.delete z) y) x
 where
	purify f x = guard (f x) *> pure x

deleteM :: Coordinate -> MCoordinateSet -> IO ()
deleteM c (MCS r iov) = modify iov (flip clearBit b) w
 where
	(w, b) = coordToIndexes c r

null :: CoordinateSet -> Bool
null = liftA2 (||) IM.null . all . liftA2 (||) IM.null $ all IS.null

size :: CoordinateSet -> Int
size = sum . fmap (sum . fmap IS.size)

-- *If* a set is connected, then this determines if the set is grounded quickly.
anyGround :: CoordinateSet -> Bool
anyGround = any (maybe False (not . IS.null) . IM.lookup 0)

-- Could be heavily optimized is y was our max-stride dimension
anyGroundM :: MCoordinateSet -> IO Bool
anyGroundM = PP.any (\(_, y, _) -> y == 0) . toListM

union :: CoordinateSet -> CoordinateSet -> CoordinateSet
union = IM.unionWith (IM.unionWith IS.union)

unions :: Foldable f => f CoordinateSet -> CoordinateSet
unions = foldl union empty

unionM :: MCoordinateSet -> MCoordinateSet -> IO ()
unionM (MCS r ld) (MCS _ rd) = do
	forM_ [0 .. resolutionSize r - 1] $ \i -> do
		rv <- read rd i
		modify ld (.|. rv) i

unions1M :: [MCoordinateSet] -> IO MCoordinateSet
unions1M []         = error "unions1M: empty list"
unions1M (h:t)      = do
	forM_ t $ \cs ->
		unionM h cs
	return h

toList :: CoordinateSet -> [Coordinate]
toList cs = do
	(x, yzs) <- IM.toList cs
	(y, zs)  <- IM.toList yzs
	(,,) x y <$> IS.toList zs

toListM :: MCoordinateSet -> Producer' Coordinate IO ()
toListM mcs@(MCS r _) =
	forM_ [0 .. r - 1] $ \x -> do
		forM_ [0 .. r - 1] $ \y -> do
			forM_ [0 .. r - 1] $ \z -> do
				let c = (x, y, z) in (lift $ memberM c mcs) >>= flip when (yield c)
