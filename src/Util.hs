module Util
	( wordToInt
	, (.>>.)
	)
where

-- base
import Data.Bits (Bits, unsafeShiftR)
import Data.Word (Word8)

wordToInt :: Word8 -> Int
wordToInt = fromInteger . toInteger

(.>>.) :: Bits b => b -> Int -> b
(.>>.) = unsafeShiftR
infixl 8 .>>.
