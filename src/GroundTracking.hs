module GroundTracking
	( GroundTracking, empty, fromModel
	, canLow, needHigh
	, fillUpdate, voidUpdate
	)
where

import Prelude hiding (GT)

-- base
import Control.Applicative (liftA2)
import Control.Monad (guard, forM_, (>=>))
import Data.List (find, partition, foldl', sortOn)
import Data.Maybe (fromMaybe)

-- pipes
import Pipes (next)

-- (this package)
import Coordinate
import CoordinateDifference
import           CoordinateSet hiding (null, empty, fromModel)
import qualified CoordinateSet as CS
import Model

-- NB: All sets represent disjoint, connected components and are non-empty
data GroundTracking = GT
	{ grounded :: [MCoordinateSet]
	, floating :: [MCoordinateSet]
	}

empty :: GroundTracking
empty = GT [] []

fromModel :: Model -> IO GroundTracking
fromModel = CS.fromModelM >=> return . flip GT [] . pure

canLow :: GroundTracking -> Bool
canLow = null . floating

needHigh :: GroundTracking -> Bool
needHigh = not . canLow

partitionM :: Monad m => (a -> m Bool) -> [a] -> m ([a], [a])
partitionM test = partitionM'
 where
	partitionM' []       = pure ([], [])
	partitionM' (x : xs) = do
		(ys, zs) <- partitionM' xs
		pass <- test x
		return $ if pass then (x : ys, zs) else (ys, x : zs)

fillUpdate :: Int -> Coordinate -> GroundTracking -> IO GroundTracking
fillUpdate r c@(_, y, _) gt = do
	(gjoining, gdisjoint) <- partitionM (inOrAdjacentM c) $ grounded gt
	(fjoining, fdisjoint) <- partitionM (inOrAdjacentM c) $ floating gt
	case (gjoining, fjoining, y) of
	 ([], [], 0) -> do
		foundation <- singletonM r c
		return GT { grounded = foundation : gdisjoint, floating = fdisjoint }
	 ([], [], _) -> do
		floater <- singletonM r c
		return GT { grounded = gdisjoint, floating = floater : fdisjoint}
	 ([], _ , 0) -> do
		settled <- unions1M fjoining
		insertM c settled
		return GT { grounded = settled : gdisjoint, floating = fdisjoint}
	 ([], _ , _) -> do
		hanging <- unions1M fjoining
		insertM c hanging
		return GT { grounded = gdisjoint, floating = hanging : fdisjoint}
	 (_ , _ , _) -> do
		support <- unions1M gjoining
		structure <- unions1M $ support : fjoining
		insertM c structure
		return GT { grounded = structure : gdisjoint, floating = fdisjoint}

insertDisjointConnected :: Int -> Coordinate -> [MCoordinateSet] -> IO [MCoordinateSet]
insertDisjointConnected r c dccs = do
	(joining, disjoint) <- partitionM (inOrAdjacentM c) dccs
	case joining of
	 [] -> flip (:) disjoint <$> singletonM r c
	 _  -> do
		joined <- unions1M joining
		insertM c joined
		return $ joined : disjoint

{-
Start with the (up to 6) adject coordinates that were in the connected
component, and try to connect the rest of the components in turn.  If at any
point you have less than 2 components, it means the component was still
connected after removing the coordinate, early-exit and use that, if anything
reamins in it.
-}
splitComponent :: Coordinate -> MCoordinateSet -> IO [MCoordinateSet]
splitComponent c cs = do
	deleteM c cs
	seeds <- adjacentsM c cs
	case seeds of
	 []  -> pure []
	 [_] -> pure [cs]
	 adj -> do
		forM_ adj (flip deleteM cs)
		mapM (singletonM r) adj >>= calcLoop (toListM cs)
 where
	r = CS.resolution cs
	calcLoop _    []  = pure []
	calcLoop _    [_] = pure [cs]
	calcLoop prod css = do
		n <- next prod
		case n of
			Left ()      -> pure css
			Right (h, t) -> insertDisjointConnected r h css >>= calcLoop t

voidUpdate :: Coordinate -> GroundTracking -> IO GroundTracking
voidUpdate c gt = do
	(gjoining, gdisjoint) <- partitionM (inOrAdjacentM c) $ grounded gt
	(fjoining, fdisjoint) <- partitionM (inOrAdjacentM c) $ floating gt
	case (gjoining, fjoining) of
	 ([], []) -> pure gt
	 ([], _ ) -> do
		joined <- unions1M fjoining
		splits <- splitComponent c joined
		pure gt { floating = splits ++ fdisjoint }
	 (_ , _) -> do
		gjoined <- unions1M gjoining
		joined <- unions1M $ gjoined : fjoining
		splits <- splitComponent c joined
		(gsplits, fsplits) <- partitionM anyGroundM splits
		return GT { grounded = gsplits ++ gdisjoint
		           , floating = fsplits ++ fdisjoint
		           }
