#!/usr/bin/env bash
set -e
BUILDTYPE="all"

if [[ ! -z $1 ]]; then
    if [[ $1 = "-d" ]]; then
        BUILDTYPE="disassemble"
    elif [[ $1 = "-a" ]]; then
        BUILDTYPE="assemble"
    elif [[ $1 = "-f" ]]; then
        BUILDTYPE="combo"
    fi
fi

# Rebuild the source every time you build the binary
bash src-dist.bash

# Absolutely positively no caching DAMNIT!
rm -rf bin-dist
mkdir bin-dist

function assemble {

    # Get ready to rumble!!!
    # Do the work for Assemble!!
    for src_model in problems/F/FA*_tgt.mdl; do
        out_file="bin-dist/$(basename ${src_model%%_tgt.mdl}).nbt"
        stack exec -- nanobuild -a "${src_model}" > "${out_file}"
        if [ $? -eq 0 ]; then
            echo "Wrote nano bot plan to assemble $(basename ${out_file})"
        fi
    done
}

function disassemble () {
    # Yes DISASSEMBLE!!!
    for src_model in problems/F/FD*_src.mdl; do
        out_file="bin-dist/$(basename ${src_model%%_src.mdl}).nbt"
        stack exec -- nanobuild -d "${src_model}" > "${out_file}"
        if [ $? -eq 0 ]; then
            echo "Wrote nano bot plan for disassemble $(basename ${out_file})"
        fi
    done
}

function both () {
    # Assemble and Dissasemble
    for src_model in problems/F/FR*_src.mdl; do
        base_model="${src_model%%_src.mdl}"
        tgt_model="${base_model}_tgt.mdl"
        out_file="bin-dist/$(basename ${base_model}).nbt"
        stack exec -- nanobuild -a "${tgt_model}" -d "${src_model}" > "${out_file}"
        if [ $? -eq 0 ]; then
            echo "Wrote nano bot plan using assembly and dissasembly $(basename ${out_file})"
        fi
    done
}
 
if [[ "${BUILDTYPE}" = "all" ]]; then
    assemble
    disassemble
    both

    echo "Made a binary distribution in bin-dist"

    zipfile="junkfood-$(date --iso-8601='minutes').zip"
    if [ -f .secret.submit ]; then
        echo "Making an encrypted zip file"
        secret=$(cat .secret.submit)
        cd bin-dist
        zip --password "$(cat ../.secret.submit)" ../"${zipfile}" *
    else
        echo "Making an insecure zip file"
        cd bin-dist
        zip ../"${zipfile}" *
    fi
    cd ..
    shasum -a 256 "${zipfile}"
elif [[ "${BUILDTYPE}" = "disassemble" ]]; then
    disassemble
    echo "Ran the disassembly files"
elif [[ "${BUILDTYPE}" = "assemble" ]]; then
    assemble
    echo "Ran the assembly files"
elif [[ "${BUILDTYPE}" = "combo" ]]; then
    both
    echo "Ran the combo files"
else
    echo "Did not run any tests"
fi
