#!/usr/bin/env bash
set -e

if [ -d src-dist ]; then
    rm -rf src-dist
fi

stack build
mkdir -p src-dist/src
mkdir -p src-dist/app
cp stack.yaml src-dist/
cp package.yaml src-dist/
cp LICENSE src-dist/
cp LICENSE.GPL3 src-dist/
cp -r src/* src-dist/src/
cp -r app/* src-dist/app/
echo "Made a source distribution in src-dist"
